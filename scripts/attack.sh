cd ./vegeta/

# max-workers: the number CCU for the load test
# duration: how long the load test will running (in minute)
cat target.http | vegeta attack -rate -0 -max-workers 80 -duration 1m | tee results.bin | vegeta report