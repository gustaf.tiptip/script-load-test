# Script Load Test

Load testing script with Vegeta (https://github.com/tsenart/vegeta)

## Install Vegeta

```
 brew install vegeta
```

***

## Running the script

```
./scripts/attack.sh
```